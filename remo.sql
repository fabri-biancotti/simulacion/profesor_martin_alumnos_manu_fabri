﻿/* creación de la base de datos */

DROP DATABASE IF EXISTS remo;
CREATE DATABASE remo;

USE remo;

/* Creación de las tablas */
  -- DIRECTIVOS
  CREATE OR REPLACE TABLE directivos(
    dni varchar(9),
    nombre_completo varchar(30) NOT NULL,
    cargo varchar(15) NOT NULL,
    PRIMARY KEY(dni)
  );


  -- CATEGORIAS
  CREATE OR REPLACE TABLE categorias(
    codigo char(2),
    nombre varchar(15) NOT NULL,
    rango_edad varchar(5) NOT NULL,
    num_remeros int,
    PRIMARY KEY(codigo)
  );


  -- ENTRENADORES 
  CREATE OR REPLACE TABLE entrenadores (
    dni varchar(9),
    nombre_completo varchar(30) NOT NULL,
    titulacion varchar(30),
    dni_directivo varchar(9) NOT NULL,
    codigo_categoria char(2) NOT NULL,
    PRIMARY KEY (dni)
  );


  -- PERMISOS CONDUCCIÓN 
  CREATE OR REPLACE TABLE permisos_conduccion (
    id_permiso int AUTO_INCREMENT,
    dni_entrenador varchar(9),
    permiso varchar(5),
    PRIMARY KEY(id_permiso)
  );


  -- REMEROS 
  CREATE OR REPLACE TABLE remeros (
    id_remero int AUTO_INCREMENT,
    codigo_categoria char(2),
    codigo_remero int,
    dni varchar(9)  NOT NULL UNIQUE,
    nombre_completo varchar(30) NOT NULL,
    fecha_nac date,
    codigo_patrocinador int,
    lesiones varchar(50),
    anios_exp int,
    datos_padre varchar(50),
    PRIMARY KEY(id_remero)
  );


  -- PATROCINADORES 
  CREATE OR REPLACE TABLE patrocinadores (
    codigo int AUTO_INCREMENT,
    nombre varchar(30) NOT NULL,
    cantidad_aportada float,
    dni_directivo varchar(9),
    PRIMARY KEY(codigo)
  );


  -- EMBARCACIONES 
  CREATE OR REPLACE TABLE embarcaciones (
    matricula varchar(6),
    nombre_tecnico varchar(12) NOT NULL,
    mote varchar(15),
    fabricante varchar(15),
    num_tripulantes int,
    necesita_patron boolean DEFAULT FALSE,
    eslora float,
    PRIMARY KEY(matricula)
  );


  -- JUEGOS DE REMOS 
  CREATE OR REPLACE TABLE juegos_remos (
    codigo int AUTO_INCREMENT,
    fabricante varchar(15) NOT NULL,
    num_remos int,
    dureza varchar(10),
    material varchar(20),
    matricula_embarcacion varchar(6),
    PRIMARY KEY (codigo)
  );


  -- PATROCINIOS 
  CREATE OR REPLACE TABLE patrocinios (
    id_patrocinio int AUTO_INCREMENT,
    codigo_patrocinador int,
    matricula_embarcacion varchar(6),
    PRIMARY KEY(id_patrocinio)
  );


  -- USOS DE EMBARCACIONES 
  CREATE OR REPLACE TABLE usos_embarcaciones (
    id_uso int AUTO_INCREMENT,
    matricula_embarcacion varchar(6),
    id_remero int,
    PRIMARY KEY (id_uso)  
  );


  -- FECHAS DE USO 
  CREATE OR REPLACE TABLE fechas_uso (
    id_fecha int AUTO_INCREMENT,
    id_uso int,
    fecha_uso date,
    PRIMARY KEY (id_fecha)  
  );



  /* CLAVES AJENAS Y RESTRICCIONES */
  -- Entrenadores
  ALTER TABLE entrenadores
    ADD CONSTRAINT fk_entrenadores_directivos
    FOREIGN KEY (dni_directivo)
    REFERENCES directivos(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    
    ADD CONSTRAINT fk_entrenadores_categorias
    FOREIGN KEY (codigo_categoria)
    REFERENCES categorias(codigo)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


  -- Permisos conduccion
  ALTER TABLE permisos_conduccion
    ADD CONSTRAINT fk_permisos_entradores
    FOREIGN KEY (dni_entrenador)
    REFERENCES entrenadores(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    
    ADD CONSTRAINT uk_permisos
    UNIQUE KEY (dni_entrenador,permiso);


  -- Remeros
  ALTER TABLE remeros
    ADD CONSTRAINT fk_remeros_categorias
    FOREIGN KEY (codigo_categoria)
    REFERENCES categorias(codigo)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    ADD CONSTRAINT fk_remeros_patrocinadores
    FOREIGN KEY (codigo_patrocinador)
    REFERENCES patrocinadores(codigo)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  
    ADD CONSTRAINT uk_remeros
    UNIQUE KEY (codigo_categoria,codigo_remero);


  -- Patrocinadores
  ALTER TABLE patrocinadores
    ADD CONSTRAINT fk_patrocinadores_directivos
    FOREIGN KEY (dni_directivo)
    REFERENCES directivos(dni)
    ON DELETE CASCADE
    ON UPDATE CASCADE;


  -- Juegos de remos
  ALTER TABLE juegos_remos
    ADD CONSTRAINT fk_remos_embarcaciones
    FOREIGN KEY (matricula_embarcacion)
    REFERENCES embarcaciones(matricula)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  
    ADD CONSTRAINT uk_matricula
    UNIQUE KEY (matricula_embarcacion);


  -- Patrocinios 
  ALTER TABLE patrocinios
    ADD CONSTRAINT fk_patrocinio_patrocinadores
    FOREIGN KEY (codigo_patrocinador)
    REFERENCES patrocinadores(codigo)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    ADD CONSTRAINT fk_patrocinio_embarcaciones
    FOREIGN KEY (matricula_embarcacion)
    REFERENCES embarcaciones(matricula)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  
    ADD CONSTRAINT uk_patrocinio
    UNIQUE KEY (codigo_patrocinador,matricula_embarcacion);


  -- Uso de embarcaciones
  ALTER TABLE usos_embarcaciones
    ADD CONSTRAINT fk_usos_embarcaciones
    FOREIGN KEY (matricula_embarcacion)
    REFERENCES embarcaciones(matricula)
    ON DELETE CASCADE
    ON UPDATE CASCADE,

    ADD CONSTRAINT fk_usos_remeros
    FOREIGN KEY (id_remero)
    REFERENCES remeros(id_remero)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  
    ADD CONSTRAINT uk_usos
    UNIQUE KEY (matricula_embarcacion,id_remero);

  -- Fechas de uso 
  ALTER TABLE fechas_uso
    ADD CONSTRAINT fk_fechas_usos
    FOREIGN KEY (id_uso)
    REFERENCES usos_embarcaciones(id_uso)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
    
    ADD CONSTRAINT uk_fechas
    UNIQUE KEY (id_uso,fecha_uso);